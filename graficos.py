
def draw_node(canvas, x, y, color):
    canvas.create_oval(x - 10, y - 10, x + 10, y + 10, fill=color)

def draw_graph(canvas, root, graph):
    # Dibujar nodos
    for node in graph:
        x, y = node
        draw_node(canvas, x, y, 'blue')
        #canvas.create_oval(x-10, y-10, x + 10, y + 10, fill='blue')

    # Dibujar aristas
    for node, connections in graph.items():
        x1, y1 = node
        for neighbor in connections:
            x2, y2 = neighbor
            canvas.create_line(x1, y1, x2, y2, fill='black', width=3)


def draw_mov(canvas, x1, y1, x2, y2):
    canvas.create_line(x1, y1, x2, y2, fill='red', width=3)


def animar_linea(canvas, x_inicial, y_inicial, x_final, y_final, duracion):
    delta_x = (x_final - x_inicial) / duracion
    delta_y = (y_final - y_inicial) / duracion
    x_actual = x_inicial
    y_actual = y_inicial

    def actualizar_linea():
        nonlocal x_actual, y_actual

        # Dibujar la línea actualizada
        canvas.delete("linea")  # Eliminar la línea anterior
        linea = canvas.create_line(x_inicial, y_inicial, x_actual, y_actual, tags="linea", width=3, fill='green')

        # Actualizar la posición de la línea
        x_actual += delta_x
        y_actual += delta_y

        if x_actual < x_final or y_actual < y_final:
            # Si aún no se ha alcanzado la posición final, programar una nueva actualización
            canvas.after(10, actualizar_linea)

    # Iniciar la animación
    actualizar_linea()

#draw_graph(mapa.mapa())