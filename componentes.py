import agente
import graficos
import mapa

import tkinter as tk

from PIL import ImageTk


actual = (0, 0)
plan = []
mapa = mapa.mapa()
inicio = ()
meta = ()
def actuador(canvas, plan):
    global actual
    mapa = sensores()
    print("actual", actual)
    x1, y1 = actual
    nodo2 = plan.pop(0)
    print("nodo2", nodo2)
    nodo = mapa.get(actual)
    print("nodo",nodo)
    arista = nodo.get(nodo2)
    print("arista", arista)
    tiempo = arista.get('time')
    x2, y2 = nodo2

    graficos.draw_mov(canvas, x1, y1, x2, y2)
    actual = nodo2
    #graficos.animar_linea(canvas, x1, y1, x2, y2, tiempo)

def sensores():
    return entorno()

def entorno():
    return mapa

def rendimiento():
    return 10


def funcionDeBusqueda():
    global mapa, inicio, meta
    res = agente.a_star(mapa, inicio, meta)
    print(res)
    return res


def comenzar():
    root = tk.Tk()
    root.geometry("960x960")  # Ajusta el tamaño de la ventana
    canvas = tk.Canvas(root, width=960, height=960)

    # Carga la imagen del mapa y ajusta su tamaño al tamaño del lienzo
    map_image = ImageTk.PhotoImage(file="mapa.jpg")
    canvas.create_image(0, 0, anchor="nw", image=map_image)
    canvas.pack()

    global inicio, meta, plan, mapa, actual
    inicio = (591, 47)
    meta = (900, 922)

    graficos.draw_graph(canvas, root, mapa)
    graficos.draw_node(canvas, *inicio, 'red')
    graficos.draw_node(canvas, *meta, 'red')
    plan = funcionDeBusqueda()
    actual = plan.pop(0)
    print(plan)

    while len(plan) > 0:
        actuador(canvas, plan)


    root.mainloop()

comenzar()

