def bidirectional_search(graph, start, goal):
    if start == goal:
        return [start]

    # Inicializar los conjuntos de exploración y explorados de ambos lados
    queue_start = [(start, [start])]
    queue_goal = [(goal, [goal])]
    explored_start = set()
    explored_goal = set()

    while queue_start and queue_goal:
        # Exploración desde el lado de inicio
        current_start, path_start = queue_start.pop(0)
        explored_start.add(current_start)

        # Verificar si el estado actual se encuentra en el lado objetivo
        if current_start in explored_goal:
            # Se encontró una intersección, se puede construir el camino completo
            intersection = current_start
            path_goal = []
            while queue_goal[-1][0] != intersection:
                path_goal.append(queue_goal[-1][0])
                queue_goal.pop()
            path_start += [intersection] + path_goal[::-1]
            return path_start

        # Expandir nodos adyacentes desde el lado de inicio
        for neighbor_start in graph[current_start]:
            if neighbor_start not in explored_start:
                queue_start.append((neighbor_start, path_start + [neighbor_start]))

        # Exploración desde el lado objetivo
        current_goal, path_goal = queue_goal.pop(0)
        explored_goal.add(current_goal)

        # Verificar si el estado actual se encuentra en el lado de inicio
        if current_goal in explored_start:
            # Se encontró una intersección, se puede construir el camino completo
            intersection = current_goal
            path_start = []
            while queue_start[-1][0] != intersection:
                path_start.append(queue_start[-1][0])
                queue_start.pop()
            path_start += [intersection] + path_goal[::-1]
            return path_start

        # Expandir nodos adyacentes desde el lado objetivo
        for neighbor_goal in graph[current_goal]:
            if neighbor_goal not in explored_goal:
                queue_goal.append((neighbor_goal, path_goal + [neighbor_goal]))

    # No se encontró un camino entre el inicio y el objetivo
    return []




# Ejemplo de uso
graph = {
    (0, 0): {(1, 0): {'distance': 1, 'time': 1}, (0, 1): {'distance': 1, 'time': 1}},
    (1, 0): {(0, 0): {'distance': 1, 'time': 1}, (1, 1): {'distance': 1, 'time': 1}, (2, 0): {'distance': 1, 'time': 1}},
    (0, 1): {(0, 0): {'distance': 1, 'time': 1}, (1, 1): {'distance': 1, 'time': 1}, (0, 2): {'distance': 1, 'time': 1}},
    (1, 1): {(1, 0): {'distance': 1, 'time': 1}, (0, 1): {'distance': 1, 'time': 1}, (1, 2): {'distance': 1, 'time': 1}, (2, 1): {'distance': 1, 'time': 1}},
    (2, 0): {(1, 0): {'distance': 1, 'time': 1}, (2, 1): {'distance': 1, 'time': 1}, (3, 0): {'distance': 1, 'time': 1}},
    (0, 2): {(0, 1): {'distance': 1, 'time': 1}, (1, 1): {'distance': 1, 'time': 1}, (0, 3): {'distance': 1, 'time': 1}},
    (1, 2): {(1, 1): {'distance': 1, 'time': 1}, (1, 3): {'distance': 1, 'time': 1}, (2, 2): {'distance': 1, 'time': 1}},
    (2, 1): {(1, 1): {'distance': 1, 'time': 1}, (2, 0): {'distance': 1, 'time': 1}, (2, 2): {'distance': 1, 'time': 1}, (3, 1): {'distance': 1, 'time': 1}},
    (3, 0): {(2, 0): {'distance': 1, 'time': 1}, (3, 1): {'distance': 1, 'time': 1}, (4, 0): {'distance': 1, 'time': 1}},
    (0, 3): {(0, 2): {'distance': 1, 'time': 1}, (1, 3): {'distance': 1, 'time': 1}, (0, 4): {'distance': 1, 'time': 1}},
    (1, 3): {(0, 3): {'distance': 1, 'time': 1}, (1, 2): {'distance': 1, 'time': 1}, (2, 3): {'distance': 1, 'time': 1}},
    (2, 2): {(1, 2): {'distance': 1, 'time': 1}, (2, 1): {'distance': 1, 'time': 1}},
    (3, 1): {(2, 1): {'distance': 1, 'time': 1}, (3, 0): {'distance': 1, 'time': 1}},
    (4, 0): {(3, 0): {'distance': 1, 'time': 1}},
    (0, 4): {(0, 3): {'distance': 1, 'time': 1}, (1, 4): {'distance': 1, 'time': 1}},
    (1, 4): {(0, 4): {'distance': 1, 'time': 1}, (1, 3): {'distance': 1, 'time': 1}, (2, 4): {'distance': 1, 'time': 1}},
    (2, 3): {(1, 3): {'distance': 1, 'time': 1}, (2, 4): {'distance': 1, 'time': 1}, (3, 3): {'distance': 1, 'time': 1}},
    (2, 4): {(1, 4): {'distance': 1, 'time': 1}, (2, 3): {'distance': 1, 'time': 1}, (3, 4): {'distance': 1, 'time': 1}},
    (3, 3): {(2, 3): {'distance': 1, 'time': 1}, (3, 4): {'distance': 1, 'time': 1}, (4, 3): {'distance': 1, 'time': 1}},
    (3, 4): {(2, 4): {'distance': 1, 'time': 1}, (3, 3): {'distance': 1, 'time': 1}, (4, 4): {'distance': 1, 'time': 1}},
    (4, 3): {(3, 3): {'distance': 1, 'time': 1}, (4, 4): {'distance': 1, 'time': 1}},
    (4, 4): {(3, 4): {'distance': 1, 'time': 1}}
}


start = (0, 0)
goal = (4, 4)

path = bidirectional_search(graph, start, goal)
print(path)

