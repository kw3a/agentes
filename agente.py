import math

import heapq
import mapa


def a_star(graph, start, goal):
    queue = []
    heapq.heappush(queue, (0, start))
    visited = set()
    parent = {}
    cost_so_far = {}
    parent[start] = None
    cost_so_far[start] = 0

    while queue:
        current_cost, current_node = heapq.heappop(queue)

        if current_node == goal:
            break

        visited.add(current_node)

        for neighbor, edge_data in graph[current_node].items():
            new_cost = cost_so_far[current_node] + edge_data['time']
            if neighbor not in cost_so_far or new_cost < cost_so_far[neighbor]:
                cost_so_far[neighbor] = new_cost
                priority = new_cost + edge_data['distance']
                heapq.heappush(queue, (priority, neighbor))
                parent[neighbor] = current_node

    path = []
    current = goal
    while current != start:
        path.append(current)
        current = parent[current]
    path.append(start)
    path.reverse()

    return path


